# Gomu

Gomu is an interpreter program for mathematics written entirely in C++.
New cool stuff can be added using modules.

For the moment, available modules are:

* base (for basic mathematics)

* garside (for computation on braid groups) 
