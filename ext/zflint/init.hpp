/**
 * This file is part of Gomu.
 *
 *  Copyright 2016 by Jean Fromentin <jean.fromentin@math.cnrs.fr>
 *
 * Gomu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gomu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gomu. If not, see <http://www.gnu.org/licenses/>. 
 */


#include "../../module.hpp"
#include "flint.hpp"
#include <sstream>

using namespace std;

//******************
//* Global objects *
//******************

extern Gomu::Type* type_ZPoly;
extern Gomu::Type* type_ZPolyFact;
extern Gomu::Type* type_ZRatFrac;
extern Gomu::Type* type_ZMatrix;
extern Gomu::Type* type_ZPolyMat;



string dispPoly(void*);
void delPoly(void*);
void* copyPoly(void*);
int cmpPoly(void*,void*);

string dispPolyFact(void*);
void delPolyFact(void*);
void* copyPolyFact(void*);
int cmpPolyFact(void*,void*);

string dispRat(void*);
void delRat(void*);
void* copyRat(void*);
int cmpRat(void*,void*);

string dispMatrix(void*);
void delMatrix(void*);
void* copyMatrix(void*);
int cmpMatrix(void*,void*);

string dispPolyMatrix(void*);
void delPolyMatrix(void*);
void* copyPolyMatrix(void*);
int cmpPolyMatrix(void*,void*);

//--- Matrix member functions ---//
void* charpoly(void*);
void* toSage(void*);
void* toPolyMatrix(void*);

//--- Polynomial member functions ---/
void* factorize(void*);


 

inline string dispPoly(void* P){return display((fmpz_poly_struct*)P);}
inline void delPoly(void* v){fmpz_poly_clear((fmpz_poly_struct*)v);}
inline int cmpPoly(void* v1,void* v2){return cmp((fmpz_poly_struct*)v1,(fmpz_poly_struct*)v2);}

inline string dispPolyFact(void* P){return display((fmpz_poly_factor_struct*)P);}
inline void delPolyFact(void* v){fmpz_poly_factor_clear((fmpz_poly_factor_struct*)v);}
inline int cmpPolyFact(void* v1,void* v2){return cmp((fmpz_poly_factor_struct*)v1,(fmpz_poly_factor_struct*)v2);}

inline string dispRat(void* R){return display((fmpz_poly_q_struct*)R);}
inline void delRat(void* v){fmpz_poly_q_clear((fmpz_poly_q_struct*)v);}
inline int cmpRat(void* v1,void* v2){return cmp((fmpz_poly_q_struct*)v1,(fmpz_poly_q_struct*)v2);}

inline string dispMatrix(void* v){return display((fmpz_mat_struct*)v);}
inline void delMatrix(void* v){fmpz_mat_clear((fmpz_mat_struct*)v);}
inline int cmpMatrix(void* v1,void* v2){return cmp((fmpz_mat_struct*)v1,(fmpz_mat_struct*)v2);}

inline string dispPolyMatrix(void* v){return display((fmpz_poly_mat_struct*)v);}
inline void delPolyMatrix(void* v){fmpz_poly_mat_clear((fmpz_poly_mat_struct*)v);}
inline int cmpPolyMatrix(void* v1,void* v2){return cmp((fmpz_poly_mat_struct*)v1,(fmpz_poly_mat_struct*)v2);} 
