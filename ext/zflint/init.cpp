/**
 * This file is part of Gomu.
 *
 *  Copyright 2016 by Jean Fromentin <jean.fromentin@math.cnrs.fr>
 *
 * Gomu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gomu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gomu. If not, see <http://www.gnu.org/licenses/>. 
 */

#include "init.hpp"

Gomu::Type* type_ZPoly;
Gomu::Type* type_ZPolyFact;
Gomu::Type* type_ZRatFrac;
Gomu::Type* type_ZMatrix;
Gomu::Type* type_ZPolyMat;

extern "C"{
  Gomu::Module::Type types[]={
    {"ZPoly",dispPoly,delPoly,copyPoly,cmpPoly,&type_ZPoly},
    {"ZPolyFact",dispPolyFact,delPolyFact,copyPolyFact,cmpPolyFact,&type_ZPolyFact},
    {"ZRatFrac",dispRat,delRat,copyRat,cmpRat,&type_ZRatFrac},
    {"ZMatrix",dispMatrix,delMatrix,copyMatrix,cmpMatrix,&type_ZMatrix},
    {"ZPolyMat",dispPolyMatrix,delPolyMatrix,copyPolyMatrix,cmpPolyMatrix,&type_ZPolyMat},
    TYPE_SENTINEL
  };

  Gomu::Module::Function memberFunctions[]={
    //ZMatrix
    {"ZPoly","charpoly",{"ZMatrix"},FUNC(charpoly)},
    {"String","toSage",{"ZMatrix"},FUNC(toSage)},
    {"ZPolyMat","toZPolyMat",{"ZMatrix"},FUNC(toPolyMatrix)},
    //ZPoly
    {"ZPolyFact","factorize",{"ZPoly"},FUNC(factorize)},
    FUNC_SENTINEL
  };
}

//**************
//* Polynomial *
//**************

void*
copyPoly(void* v){
  fmpz_poly_struct* res=new fmpz_poly_struct;
  fmpz_poly_init(res);
  fmpz_poly_set(res,(fmpz_poly_struct*)v);
  return res;
}

//*********************
//* Polynomial factor *
//*********************

void*
copyPolyFact(void* v){
  fmpz_poly_factor_struct* res=new fmpz_poly_factor_struct;
  fmpz_poly_factor_init(res);
  fmpz_poly_factor_set(res,(fmpz_poly_factor_struct*)v);
  return res;
}

//*************
//* Rationnal *
//*************

void*
copyRat(void* v){
  fmpz_poly_q_struct* res=new fmpz_poly_q_struct;
  fmpz_poly_q_init(res);
  fmpz_poly_q_set(res,(fmpz_poly_q_struct*)v);
  return res;
}
//**********
//* Matrix *
//**********

void* copyMatrix(void* v){
  fmpz_mat_struct* res=new fmpz_mat_struct;
  fmpz_mat_init_set(res,(fmpz_mat_struct*)v);
  return res;
}


//*********************
//* Polynomial Matrix *
//*********************

void* copyPolyMatrix(void* v){
  fmpz_poly_mat_struct* res=new fmpz_poly_mat_struct;
  fmpz_poly_mat_init_set(res,(fmpz_poly_mat_struct*)v);
  return res;
}

//***************************
//* Matrix member functions *
//***************************

void* charpoly(void* v){
  fmpz_poly_struct* res=new fmpz_poly_struct;
  fmpz_poly_init(res);
  fmpz_mat_charpoly(res,(fmpz_mat_struct*)v);
  return res;
}

void* toSage(void* v){
  ostringstream os;
  dispSage(os,(fmpz_mat_struct*)v);
  return new string(os.str());
}

void* toPolyMatrix(void* v){
  fmpz_poly_mat_struct* res=new fmpz_poly_mat_struct;
  fmpz_poly_mat_init_set(res,(fmpz_mat_struct*) v);
  return res;
};


//*******************************
//* Polynomial member functions *
//*******************************

void* factorize(void* v){
  fmpz_poly_factor_struct* res=new fmpz_poly_factor_struct;
  fmpz_poly_factor_init(res);
  fmpz_poly_factor_zassenhaus(res,(fmpz_poly_struct*)v);
  return res;
}
