/**
 * This file is part of Gomu.
 *
 *  Copyright 2016 by Jean Fromentin <jean.fromentin@math.cnrs.fr>
 *
 * Gomu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gomu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gomu. If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef FLINT_HPP
#define FLINT_HPP

#include <iostream>
#include "flint/fmpz.h"
#include "flint/fmpz_mat.h"
#include "flint/fmpz_poly.h"
#include "flint/fmpz_poly_q.h"
#include "flint/fmpz_poly_mat.h"

using namespace std;


ostream& operator<<(ostream&,const fmpz_t);
ostream& operator<<(ostream&,const fmpz_mat_t);
ostream& operator<<(ostream&,const fmpz_poly_t);
ostream& operator<<(ostream&,const fmpz_poly_factor_t);
ostream& operator<<(ostream&,const fmpz_poly_mat_t);
string display(fmpz*);
string display(fmpz_poly_struct*);
string display(fmpz_poly_factor_struct*);
string display(fmpz_poly_q_struct*);
string display(fmpz_mat_struct*);
string display(fmpz_poly_mat_struct*);


string toHtml(const fmpz_t);//obsolete
string toHtml(const fmpz_poly_t);
string toHtml(const fmpz_poly_q_t);
string toHtml(const fmpz_poly_factor_t);
int cmp(fmpz_mat_struct*,fmpz_mat_struct*);
int cmp(fmpz_poly_struct*,fmpz_poly_struct*);
int cmp(fmpz_poly_q_struct*,fmpz_poly_q_struct*);
int cmp(fmpz_poly_factor_struct*,fmpz_poly_factor_struct*);
int cmp(fmpz_poly_mat_struct*,fmpz_poly_mat_struct*);
fmpz* get(const fmpz_mat_t,slong,slong);
fmpz_poly_struct* get(const fmpz_poly_mat_t,slong,slong);
void fmpz_poly_mat_init_set(fmpz_poly_mat_t,fmpz_mat_t);
void dispSage(ostream&,const fmpz_mat_t);


inline ostream& operator<<(ostream& os,const fmpz_t z){
  char* str=fmpz_get_str(NULL,10,z);  
  os<<str;
  free(str);
  return os;
}


inline ostream& operator<<(ostream& os,const fmpz_poly_t P){
  char* str=fmpz_poly_get_str_pretty(P,"x");
  os<<str;
  free(str);
  return os;
}

inline string toHtml(const fmpz_t z){
  char* str=fmpz_get_str(NULL,10,z);  
  string res(str);
  return res;
}


inline fmpz* get(const fmpz_mat_t A,slong i,slong j){
  return fmpz_mat_entry(A,i,j);
}

inline fmpz_poly_struct* get(const fmpz_poly_mat_t A,slong i,slong j){
  return fmpz_poly_mat_entry(A,i,j);
}


#endif
