#include "init.hpp"

//******************
//* Global objects *
//******************

Gomu::Type* type_PermutationA;
Gomu::Type* type_PermutationB;
Gomu::Type* type_PermutationD;
Gomu::Type* type_PermutationEnumeratorA;
Gomu::Type* type_PermutationEnumeratorB;
Gomu::Type* type_PermutationEnumeratorD;

//*************************
//* Extension inilisation *
//*************************

extern "C"{
  Gomu::Module::Type types[]={
    {"PermutationA",Permutations::dispPerm<CoxeterA>,Permutations::delPerm<CoxeterA>,Permutations::copyPerm<CoxeterA>,Permutations::cmpPerm<CoxeterA>,&type_PermutationA},
    {"PermutationB",Permutations::dispPerm<CoxeterB>,Permutations::delPerm<CoxeterB>,Permutations::copyPerm<CoxeterB>,Permutations::cmpPerm<CoxeterB>,&type_PermutationB},
    {"PermutationD",Permutations::dispPerm<CoxeterD>,Permutations::delPerm<CoxeterD>,Permutations::copyPerm<CoxeterD>,Permutations::cmpPerm<CoxeterD>,&type_PermutationD},
    {"PermutationEnumeratorA",Permutations::dispPermE<CoxeterA>,Permutations::delPermE<CoxeterA>,Permutations::copyPermE<CoxeterA>,Permutations::cmpPermE<CoxeterA>,&type_PermutationEnumeratorA},
    {"PermutationEnumeratorB",Permutations::dispPermE<CoxeterB>,Permutations::delPermE<CoxeterB>,Permutations::copyPermE<CoxeterB>,Permutations::cmpPermE<CoxeterB>,&type_PermutationEnumeratorB},
    {"PermutationEnumeratorD",Permutations::dispPermE<CoxeterD>,Permutations::delPermE<CoxeterD>,Permutations::copyPermE<CoxeterD>,Permutations::cmpPermE<CoxeterD>,&type_PermutationEnumeratorD},
   TYPE_SENTINEL
  };

  //--- Functions ---//
  Gomu::Module::Function functions[]={
    {"PermutationA","permutationA",{"Integer"},(void*)Permutations::intToPerm<CoxeterA>},
    {"PermutationB","permutationB",{"Integer"},(void*)Permutations::intToPerm<CoxeterB>},
    {"PermutationD","permutationD",{"Integer"},(void*)Permutations::intToPerm<CoxeterD>},
    {"PermutationEnumeratorA","permutationEnumeratorA",{"Integer"},(void*)Permutations::intToPermE<CoxeterA>},
    {"PermutationEnumeratorB","permutationEnumeratorB",{"Integer"},(void*)Permutations::intToPermE<CoxeterB>},
    {"PermutationEnumeratorD","permutationEnumeratorD",{"Integer"},(void*)Permutations::intToPermE<CoxeterD>},
    FUNC_SENTINEL
  };

  //--- Member functions ---//
  Gomu::Module::Function member_functions[]={
    //--- PermutationEnumertor ---//
    {"PermutationA","get",{"PermutationEnumeratorA"},(void*)Permutations::PE_get<CoxeterA>},
    {"PermutationB","get",{"PermutationEnumeratorB"},(void*)Permutations::PE_get<CoxeterB>},
    {"PermutationD","get",{"PermutationEnumeratorD"},(void*)Permutations::PE_get<CoxeterD>},
    {"Boolean","next",{"PermutationEnumeratorA"},(void*)Permutations::PE_next<CoxeterA>},
    {"Boolean","next",{"PermutationEnumeratorB"},(void*)Permutations::PE_next<CoxeterB>},
    {"Boolean","next",{"PermutationEnumeratorD"},(void*)Permutations::PE_next<CoxeterD>},
    {"Void","reset",{"PermutationEnumeratorA"},(void*)Permutations::PE_reset<CoxeterA>},
    {"Void","reset",{"PermutationEnumeratorB"},(void*)Permutations::PE_reset<CoxeterB>},
    {"Void","reset",{"PermutationEnumeratorD"},(void*)Permutations::PE_reset<CoxeterD>},
    {"Integer","size",{"PermutationEnumeratorA"},(void*)Permutations::PE_size<CoxeterA>},
    {"Integer","size",{"PermutationEnumeratorB"},(void*)Permutations::PE_size<CoxeterB>},
    {"Integer","size",{"PermutationEnumeratorD"},(void*)Permutations::PE_size<CoxeterD>},
    FUNC_SENTINEL
  };
}
