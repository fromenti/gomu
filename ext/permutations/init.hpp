/**
 * This file is part of Gomu.
 *
 *  Copyright 2016 by Jean Fromentin <jean.fromentin@math.cnrs.fr>
 *
 * Gomu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gomu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gomu. If not, see <http://www.gnu.org/licenses/>. 
 */

#include "../../module.hpp"
#include "permutations.hpp"

using namespace std;

//*****************
//* Global object *
//*****************

extern Gomu::Type* type_PermutationA;
extern Gomu::Type* type_PermutationB;
extern Gomu::Type* type_PermutationD;
extern Gomu::Type* type_PermutationEnumeratorA;
extern Gomu::Type* type_PermutationEnumeratorB;
extern Gomu::Type* type_PermutationEnumeratorD;


namespace Permutations{
  template<CoxeterType T> string dispPerm(void*);
  template<CoxeterType T> void delPerm(void*);
  template<CoxeterType T> void* copyPerm(void*);
  template<CoxeterType T> int cmpPerm(void*,void*);

  template<CoxeterType T> string dispPermE(void*);
  template<CoxeterType T> void delPermE(void*);
  template<CoxeterType T> void* copyPermE(void*);
  template<CoxeterType T> int cmpPermE(void*,void*);


  //--- Constructors ---//
  template<CoxeterType T> void* intToPerm(void*);
  template<CoxeterType T> void* intToPermE(void*);

  //--- Permutation functions ---//

  //--- Permutation enumerator functions ---//
  template<CoxeterType T> void* PE_get(void*);
  template<CoxeterType T> void* PE_next(void*);
  template<CoxeterType T> void* PE_reset(void*);
  template<CoxeterType T> void* PE_size(void*);
}


namespace Permutations{
  template<CoxeterType T> inline string dispPerm(void* P){return ((Permutation<T>*)P)->display();}
  template<CoxeterType T> inline void delPerm(void* v){delete ((Permutation<T>*)v);}
  template<CoxeterType T> inline void* copyPerm(void* v){return new Permutation<T>(*((Permutation<T>*)v));}
  template<CoxeterType T> inline int cmpPerm(void* v1,void* v2){return ((Permutation<T>*)v1)->cmp(*(Permutation<T>*)v2);}

  template<CoxeterType T> inline string dispPermE(void* P){return ((PermutationEnumerator<T>*)P)->display();}
  template<CoxeterType T> inline void delPermE(void* v){delete ((PermutationEnumerator<T>*)v);}
  template<CoxeterType T> inline void* copyPermE(void* v){return new PermutationEnumerator<T>(*((PermutationEnumerator<T>*)v));}
  template<CoxeterType T> inline int cmpPermE(void* v1,void* v2){return ((PermutationEnumerator<T>*)v1)->cmp(*(PermutationEnumerator<T>*)v2);}

  //--- Constructors ---//
  template<CoxeterType T>
  inline void*
  intToPerm(void* z){
    slong n=Gomu::get_slong(z);
    if(n<0 or n>15) RuntimeError("A postive integer < 16 is needed");
    return new Permutation<T>(n);
  }

  template<CoxeterType T>
  inline void*
  intToPermE(void* z){
    slong n=Gomu::get_slong(z);
    if(n<0 or n>15) RuntimeError("A postive integer < 16 is needed");
    return new PermutationEnumerator<T>(n);
  }

  //***********************************
  //* PermutationEnumerator functions *
  //***********************************
  template<CoxeterType T> inline void* PE_get(void* PE){return new Permutation<T>(((PermutationEnumerator<T>*)PE)->get());}
  template<CoxeterType T> inline void* PE_next(void* PE){return Gomu::to_boolean(((PermutationEnumerator<T>*)PE)->next());}
  template<CoxeterType T> inline void* PE_reset(void* PE){((PermutationEnumerator<T>*)PE)->reset();return nullptr;}
  template<CoxeterType T> inline void* PE_size(void* PE){return Gomu::to_integer(((PermutationEnumerator<T>*)PE)->size());}
  
}
